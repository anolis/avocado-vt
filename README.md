# 安装

```
dnf install xz tcpdump iproute iputils gcc glibc-headers nc git
git clone https://gitee.com/anolis/avocado-vt.git
cd avocado-vt
pip install .
```
# 初始化

```
# qemu, libvirt, v2v, openvswitch, etc
avocado vt-bootstrap --vt-type qemu
```

# 查看测试用例列表

```
# qemu, libvirt, v2v, openvswitch, etc
avocado list --vt-type qemu
```

# 运行

```
# 用例名称参考上条展示出来的内容
avocado run type_specific.io-github-autotest-qemu.bridge_mirror
```

# 参考文档
http://avocado-vt.readthedocs.org/en/latest/GetStartedGuide.html
